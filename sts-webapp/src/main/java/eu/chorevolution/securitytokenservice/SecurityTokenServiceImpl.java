/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice;

import org.apache.cxf.binding.soap.saaj.SAAJFactoryResolver;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.sts.QNameConstants;

import java.net.UnknownHostException;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenCollectionType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseCollectionType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.Action;
import javax.xml.ws.soap.SOAPFaultException;

import eu.chorevolution.securitytokenservice.federationserver.api.ServicesDao;
import eu.chorevolution.securitytokenservice.federationserver.api.UsersDao;
import eu.chorevolution.securitytokenservice.operation.IssueOperation;
import eu.chorevolution.securitytokenservice.operation.ValidateOperation;

/**
 * @author Frederic Motte
 * The implementation of the SecurityTokenService
 */
@WebService(targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/wsdl", 
name = "SecurityTokenService")
@XmlSeeAlso({org.apache.cxf.ws.security.sts.provider.model.ObjectFactory.class,
	org.apache.cxf.ws.security.sts.provider.model.wstrust14.ObjectFactory.class,
	org.apache.cxf.ws.security.sts.provider.model.secext.ObjectFactory.class,
	org.apache.cxf.ws.security.sts.provider.model.utility.ObjectFactory.class, 
	org.apache.cxf.ws.security.sts.provider.model.xmldsig.ObjectFactory.class,
	org.apache.cxf.ws.addressing.ObjectFactory.class })
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public class SecurityTokenServiceImpl {

	/**
	 * The database wrapper
	 */
	private UsersDao userDao;
	private ServicesDao serviceDao;

	private String masterPasswordKey;
	private String applicationKeyPassword;
	
	private static final Logger logger = LoggerFactory.getLogger(SecurityTokenServiceImpl.class);
	
	/**
	 * Constructor
	 */
	public SecurityTokenServiceImpl() {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * Exception unsupported operation throw when the operation is not coded
	 * @param string input
	 */
	private void throwUnsupportedOperation(String string) {
		SOAPFault fault;
		try {
			fault = SAAJFactoryResolver.createSOAPFactory(null).createFault();
			fault.setFaultString("Unsupported operation " + string);
			throw new SOAPFaultException(fault);
		} catch (SOAPException e) {
			throw new Fault(e);
		}
	}

	@WebResult(name = "RequestSecurityTokenResponse",
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512", 
			partName = "response")
	@Action(input = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/KET", 
	output = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/KETFinal")
	@WebMethod(operationName = "KeyExchangeToken")
	public RequestSecurityTokenResponseType keyExchangeToken(
			@WebParam(partName = "request", 
			name = "RequestSecurityToken", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
			RequestSecurityTokenType request
			)
	{
		logger.debug("KeyExchangeToken");
		throwUnsupportedOperation("KeyExchangeToken");
		return null;
	};

//	@WebResult(name = "RequestSecurityTokenResponseCollection",
//			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512",
//			partName = "responseCollection")
//	@Action(input = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue", 
//	output = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTRC/IssueFinal")
//	@WebMethod(operationName = "Issue")
//	public
//	RequestSecurityTokenResponseCollectionType issue(
//			@WebParam(partName = "request",
//			name = "RequestSecurityToken", 
//			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
//			RequestSecurityTokenType request
//			)
//	{
//		logger.debug("Issue");
////		RequestSecurityTokenResponseType response=issueSingle(request);
////		RequestSecurityTokenResponseCollectionType responseCollection=QNameConstants.WS_TRUST_FACTORY.createRequestSecurityTokenResponseCollectionType();
////		responseCollection.getRequestSecurityTokenResponse().add(response);
////		return responseCollection;
//		
//		throwUnsupportedOperation("Issue");
//		return null;
//	};

	@WebResult(name = "RequestSecurityTokenResponse",
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512",
			partName = "response")
	@Action(input = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue", 
	output = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTRC/IssueFinal")
	@WebMethod(operationName = "IssueSingle")
	public
	RequestSecurityTokenResponseType issueSingle(
			@WebParam(partName = "request",
			name = "RequestSecurityToken", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
			RequestSecurityTokenType request
			)
	{
		logger.debug("IssueSingle");

		//create validate operation
		IssueOperation op = new IssueOperation(userDao, serviceDao, masterPasswordKey, applicationKeyPassword);
		
		// validate the request
		RequestSecurityTokenResponseType res = op.issue(request);
		return res;
	};

	@WebResult(name = "RequestSecurityTokenResponse", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512", 
			partName = "response")
	@Action(input = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Cancel", 
	output = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/CancelFinal")
	@WebMethod(operationName = "Cancel")
	public
	RequestSecurityTokenResponseType cancel(
			@WebParam(partName = "request", name = "RequestSecurityToken", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
			RequestSecurityTokenType request
			)
	{
		logger.debug("Cancel");

		throwUnsupportedOperation("Cancel");
		return null;
	};

	@WebResult(name = "RequestSecurityTokenResponse", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512", 
			partName = "response")
	@Action(input = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Validate", 
	output = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/ValidateFinal")
	@WebMethod(operationName = "Validate")
	public
	RequestSecurityTokenResponseType validate(
			@WebParam(partName = "request", name = "RequestSecurityToken", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
			RequestSecurityTokenType request
			)
	{
		logger.debug("Validate");

		//create validate operation
		ValidateOperation op = new ValidateOperation(userDao, serviceDao, masterPasswordKey);
		
		// validate the request
		RequestSecurityTokenResponseType res = op.validate(request);
		return res;
	};

	@WebResult(name = "RequestSecurityTokenResponseCollection", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512",
			partName = "responseCollection")
	@WebMethod(operationName = "RequestCollection")
	public
	RequestSecurityTokenResponseCollectionType requestCollection(
			@WebParam(partName = "requestCollection",
			name = "RequestSecurityTokenCollection",
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
			RequestSecurityTokenCollectionType requestCollection
			)
	{
		logger.debug("RequestCollection");
		
		throwUnsupportedOperation("RequestCollection");
		return null;
	};

	@WebResult(name = "RequestSecurityTokenResponse", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512", 
			partName = "response")
	@Action(input = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Renew", 
	output = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RSTR/RenewFinal")
	@WebMethod(operationName = "Renew")
	public
	RequestSecurityTokenResponseType renew(
			@WebParam(partName = "request", 
			name = "RequestSecurityToken", 
			targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512")
			RequestSecurityTokenType request
			)
	{
		logger.debug("Renew");

		return issueSingle(request);
	};

	/**
	 * Set the UserDao (used by spring)
	 * @param userDao userDAO
	 */
	public void setUserDao(final UsersDao userDao) {
		logger.debug("setUserDao");

		if (userDao == null)
		{
			logger.error("UserDao is NULL");
		}
		this.userDao = userDao;
	}
	
	public void setMasterPasswordKey(String masterPasswordKey) {
		this.masterPasswordKey = masterPasswordKey;
	}

	public void setApplicationKeyPassword(String applicationKeyPassword) {
		this.applicationKeyPassword = applicationKeyPassword;
	}

	/**
	 * Set the ServiceDao (used by spring)
	 * @param serviceDao serviceDAO
	 */
	public void setServiceDao(final ServicesDao serviceDao) {
		logger.debug("setServiceDao");

		if (serviceDao == null)
		{
			logger.error("serviceDao is NULL");
		}
		this.serviceDao = serviceDao;
	}
		
	/**
	 * Init the component (used by spring)
	 * @throws UnknownHostException
	 */
	public void init() throws UnknownHostException {
		logger.debug("SecurityTokenServiceImpl init");
	}

	/**
	 * Destroy the component (used by spring) 
	 */
	public void teardown() {
		logger.debug("SecurityTokenServiceImpl teardown");
	}

}
