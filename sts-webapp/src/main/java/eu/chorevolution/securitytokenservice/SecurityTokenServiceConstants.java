/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice;

import javax.xml.namespace.QName;

public class SecurityTokenServiceConstants {

	public static final QName SECURITY_VALIDATION_DOMAIN = new QName("http://chorevolution.eu/STS" , "SecurityValidationDomain");
	public static final QName SECURITY_ISSUER_DOMAIN = new QName("http://chorevolution.eu/STS" , "SecurityIssuerDomain");

	public static final QName SECURITY_CONSUMER_TYPE = new QName("http://chorevolution.eu/STS" , "SecurityConsumerType");
	public static final QName SECURITY_PROVIDER_TYPE = new QName("http://chorevolution.eu/STS" , "SecurityProviderType");

	public static final QName SECURITY_PROVIDER_NAME = new QName("http://chorevolution.eu/STS" , "SecurityProviderName");
}
