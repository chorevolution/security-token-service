/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.client;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.ws.security.WSSecurityException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;
import eu.chorevolution.securitytokenservice.security.Encryptor;

public class test {
	public static void main(String[] args) throws WSSecurityException {			
		EndUser user = new EndUser();				
		
		Map.Entry<String, String> e = new  AbstractMap.SimpleEntry<>("exmpleString", "zecdzec");
		
		user.getServiceCredentials().put("Myservice", e);
		
		Map<String, Set<String>> attr = user.getAttributes();
		
		ObjectMapper om = new ObjectMapper();
		try {
			System.out.println(om.writeValueAsString(user));
		} catch (JsonProcessingException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String masterKeyPassword="dsadsadasa4444";
		String applicationKeyPassword="dsadsadasa4444";
//		String applicationKeyPassword="skjajsjsjljkdsa6633";
				
				
		Encryptor masterPasswordEncryptor = new Encryptor(masterKeyPassword);
		Encryptor applicationPasswordEncryptor = new Encryptor(applicationKeyPassword);
		
		try {
			
			String masterpassword1 = masterPasswordEncryptor.encode("password1");
			String masterpassword2 = masterPasswordEncryptor.encode("password2");
			
			String password1 = applicationPasswordEncryptor.encode("password1");
			String password2 = applicationPasswordEncryptor.encode("password2");
			
			System.out.println(masterpassword1);
			System.out.println(masterpassword2);
			System.out.println(password1);
			System.out.println(password2);
			
			
//			String user1 = "{\"username\":\"test1\",\"password\":\""+masterpassword1+"\",\"active\":true,\"choreographies\":[\"wp5\"],\"groups\":[\"group2\",\"group1\"],\"serviceCredentials\":{\"sc1\":{\"username\":\"sc1_test\",\"password\":\""+password1+"\"},\"sc3\":{\"username\":\"sc2_test\",\"password\":\""+password2+"\"}},\"attributes\":{\"attr2\":[\"value4\",\"value3\"],\"attr1\":[\"value2\",\"value1\"]}}";
//			String user2 = "{\"username\":\"test2\",\"password\":\""+masterpassword2+"\",\"active\":true,\"choreographies\":[\"wp5\"],\"groups\":[\"group3\",\"group4\"],\"serviceCredentials\":{\"sc1\":{\"username\":\"sc1_test\",\"password\":\""+password1+"\"},\"sc3\":{\"username\":\"sc2_test\",\"password\":\""+password2+"\"}},\"attributes\":{\"attr2\":[\"value4\",\"value3\"],\"attr1\":[\"value2\",\"value1\"]}}";
			String user1 = "{\"username\":\"test1\",\"password\":\""+masterpassword1+"\",\"active\":true,\"choreographies\":[\"wp5\"],\"groups\":[\"group2\",\"group1\"],\"serviceCredentials\":{\"sc1\":{\"sc1_test\":\""+password1+"\"},\"sc3\":{\"sc2_test\":\""+password2+"\"}},\"attributes\":{\"attr2\":[\"value4\",\"value3\"],\"attr1\":[\"value2\",\"value1\"]}}";
			String user2 = "{\"username\":\"test2\",\"password\":\""+masterpassword2+"\",\"active\":true,\"choreographies\":[\"wp5\"],\"groups\":[\"group3\",\"group4\"],\"serviceCredentials\":{\"sc1\":{\"sc1_test\":\""+password1+"\"},\"sc3\":{\"sc2_test\":\""+password2+"\"}},\"attributes\":{\"attr2\":[\"value4\",\"value3\"],\"attr1\":[\"value2\",\"value1\"]}}";

			String service1 = "{\"servicename\":\"service1\",\"serviceaccount\":\"root1\",\"credentialtype\":\"usernametoken\",\"credential\":\""+password1+"\"}";
			String service2 = "{\"servicename\":\"service2\",\"serviceaccount\":\"root2\",\"credentialtype\":\"usernametoken\",\"credential\":\""+password2+"\"}";

			System.out.println(user1);
			System.out.println(user2);
			System.out.println(service1);
			System.out.println(service2);

			
			

			test t = new test();
			//t.postUser("domain3", user1);
			user1 = "{\"username\":\"test1\",\"password\":\""+masterpassword1+"\",\"active\":true,\"choreographies\":[\"wp5\"],\"groups\":[\"group2\",\"group1\"],\"serviceCredentials\":{\"sc1\":{\"sc1_test\":\""+password1+"\"},\"sc3\":{\"sc2_test\":\""+password2+"\"}},\"attributes\":{\"attr2\":[\"value4\",\"value3\"],\"attr1\":[\"value2\",\"value1\"]}}";
			
			t.postUser("Master", user1);
						t.postUser("Master", user2);
//			t.postService("domain3", service1);
//			t.postService("domain3", service2);

			
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
//		} catch(WSSecurityException e){
//			e.printStackTrace();
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
		} catch (BadPaddingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

//		Client client = Client.create();
//
//		WebResource webResource = client
//				.resource("http://localhost:8080/SecurityTokenService/resources/domains/domain2/endusers/test");
//
//		ClientResponse response = webResource.accept("application/json")
//				.get(ClientResponse.class);
//
//		if (response.getStatus() != 200) {
//			throw new WSSecurityException("Unauthorized to access the service. No user information found");
//		}
//
//		String output = response.getEntity(String.class);
//
//		System.out.println("Output from Server .... \n");
//		System.out.println(output);
//		Gson gson = new Gson();
//
//
//		EndUser userRes = gson.fromJson(output, EndUser.class);
//		System.out.println(userRes.getPassword());
//		System.out.println(userRes.getUsername());
//
//
//		userRes.setUsername("test2");
//
//		webResource = client
//				.resource("http://localhost:8080/SecurityTokenService/resources/domains/domain2/endusers/");
//
//		response = webResource.accept("application/json").type("application/json")
//				.post(ClientResponse.class, gson.toJson(userRes, EndUser.class));

	}

	public void postUser(String domain, String user) throws WSSecurityException{
                Client client = ClientBuilder.newClient();

                WebTarget webResource = client.target("http://localhost:8080/SecurityTokenService/").
                            path("resources/domains/"+domain+"/endusers/");
		Response response= webResource.request("application/json").
                        post(Entity.entity(MediaType.APPLICATION_JSON_TYPE, user));
		System.out.println("Post User " + response.getStatus());
		if (response.getStatus() != 200 && response.getStatus() != 204 && response.getStatus() != 201 ) {
			throw new WSSecurityException("problem during post user " + user);
		}
	}
	public void putUser(String domain, String user) throws WSSecurityException{
                Client client = ClientBuilder.newClient();

                WebTarget webResource = client.target("http://localhost:8080/SecurityTokenService/").
                            path("resources/domains/"+domain+"/endusers/test1");
		Response response= webResource.request("application/json").
                        put(Entity.entity(MediaType.APPLICATION_JSON_TYPE, user));
		System.out.println("Put User " + response.getStatus());
		if (response.getStatus() != 200 && response.getStatus() != 204 && response.getStatus() != 201 ) {
			throw new WSSecurityException("problem during put user " + user);
		}
	}

		public void postService(String domain, String service) throws WSSecurityException{
                Client client = ClientBuilder.newClient();

                WebTarget webResource = client.target("http://localhost:8080/SecurityTokenService/").
                            path("resources/domains/"+domain+"/services");
		Response response= webResource.request("application/json").
                        put(Entity.entity(MediaType.APPLICATION_JSON_TYPE, service));
			System.out.println("Post service " + response.getStatus());
			if (response.getStatus() != 200 && response.getStatus() != 204&& response.getStatus() != 201) {
				throw new WSSecurityException("problem during post user " + service);
			}
		}


	}
