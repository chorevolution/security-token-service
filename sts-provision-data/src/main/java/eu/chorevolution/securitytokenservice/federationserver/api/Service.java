/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

/**
 * @author Frederic Motte
 * EndUser repesente the user into the rest request
 */
@XmlRootElement(name = "service")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = { "servicename", "serviceaccount", "credentialtype", "credential" })
public class Service implements Serializable, Comparable<Service> {

    private static final long serialVersionUID = -3901863997664130718L;

    // CHECKSTYLE:OFF
    private String _id;
    // CHECKSTYLE:ON

    private String servicename;

    private String serviceaccount;

    private String credentialtype;

    private String credential;

    // CHECKSTYLE:OFF
    @XmlTransient
    public String get_id() {
        return _id;
    }

    public void set_id(final String _id) {
        this._id = _id;
    }
    // CHECKSTYLE:ON

    @XmlElement(name = "servicename")
    public String getServicename() {
        return servicename;
    }

    public void setServicename(final String servicename) {
        this.servicename = servicename;
    }

    @XmlElement(name = "serviceaccount")
    public String getServiceaccount() {
        return serviceaccount;
    }

    public void setServiceaccount(final String serviceaccount) {
        this.serviceaccount = serviceaccount;
    }

    @XmlElement(name = "credentialtype")
    public String getCredentialtype() {
        return credentialtype;
    }

    public void setCredentialtype(final String credentialtype) {
        this.credentialtype = credentialtype;
    }

    @XmlElement(name = "credential")
    public String getCredential() {
        return credential;
    }

    public void setCredential(final String credential) {
        this.credential = credential;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Service)) {
            return false;
        }
        Service that = (Service) o;
        return Objects.equal(this.servicename, that.servicename);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(servicename);
    }

    @SuppressWarnings("deprecation")
    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("servicename", servicename)
                .add("serviceaccount", serviceaccount)
                .add("credentialtype", credentialtype)
                .add("credential", credential)
                .toString();
    }

    @Override
    public int compareTo(final Service that) {
        return ComparisonChain.start()
                .compare(this.servicename, that.servicename)
                .result();
    }
}
