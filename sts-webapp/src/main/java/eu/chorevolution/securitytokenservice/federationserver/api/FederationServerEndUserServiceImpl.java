/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Path;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;

import eu.chorevolution.securitytokenservice.domain.builder.UserBuilder;
@Path("/domains/{domain}/endusers")
public class FederationServerEndUserServiceImpl implements FederationServerEndUserService {

    private static final Logger logger = LoggerFactory.getLogger(FederationServerEndUserService.class);

    public static final String USER_NOT_FOUND_MSG = "User with username[%s] does not exist";

    public static final String USER_EXISTS_MSG = "User with username[%s] already exists";

    private UsersDao usersDao;

    @Autowired
    public FederationServerEndUserServiceImpl(UsersDao usersDao) {
        logger.debug("FederationServerEndUserServiceImpl Constructor");
        this.usersDao = usersDao;
    }

    @Override
    public Response list(String domain) {
        logger.debug("Get Users for the domain " + domain);
        List<EndUser> books = new ArrayList<>(usersDao.readAll(domain));
        GenericEntity<List<EndUser>> booksEntity = new GenericEntity<List<EndUser>>(books) {
        };
        return Response.ok(booksEntity).build();
    }

    @Override
    public Response create(String domain, EndUser enduser) {
        logger.debug("Add a new user into the domain " + domain);
        if (usersDao.create(domain, enduser).isPresent()) {
            return Response.status(CREATED).build();
        }
        return Response.status(CONFLICT)
                .entity(format(USER_EXISTS_MSG, enduser.getUsername()))
                .type(MediaType.TEXT_PLAIN).build();
    }

    @Override
    public Response read(String domain, String username) {
        logger.debug("Get User information for user " + username + " into the domain " + domain);
        Optional<EndUser> optional = usersDao.readByUsername(domain, username);
        if (optional.isPresent()) {
            return Response.ok(optional.get()).build();
        }
        return Response.status(NOT_FOUND).entity(format(USER_NOT_FOUND_MSG, username)).build();
    }

    @Override
    public Response update(String domain, String username, EndUser enduser) {
        logger.debug("Modify User information for user " + username + " into the domain " + domain);
        Optional<EndUser> optional = usersDao.update(domain, enduser);
        if (optional.isPresent()) {
            return Response.status(NO_CONTENT).build();
        }
        return Response.status(NOT_FOUND).entity(format(USER_NOT_FOUND_MSG, enduser.getUsername())).build();

    }

    @Override
    public Response delete(String domain, String username) {
        logger.debug("Delete User " + username + " into the domain " + domain);

        if (usersDao.delete(domain, new UserBuilder(username).build())) {
            return Response.status(NO_CONTENT).build();
        }
        return Response.status(NOT_FOUND).entity(format(USER_NOT_FOUND_MSG, username)).build();
    }

}
