/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;

import eu.chorevolution.securitytokenservice.domain.builder.ServiceBuilder;
import eu.chorevolution.securitytokenservice.util.AppUtils;

/**
 * @author Frederic Motte
 * ServicesDao Wrap the usage of the MongoDB database for the endservice information
 */
public class ServicesDao {
	private static final Logger logger = LoggerFactory.getLogger(ServicesDao.class);

	private MongoClient mongoClient;
	private String dbName;
	private Map<String, DBCollection> servicesCollection;

	public ServicesDao() {
	}

	/**
	 * Initialization of the component (used by Spring)
	 * @throws UnknownHostException
	 */
	public void init() throws UnknownHostException {
		logger.debug("ServiceDao init");
		DB serviceshelfDatabase = mongoClient.getDB(dbName);
		Set<String> list = serviceshelfDatabase.getCollectionNames();
		servicesCollection = new HashMap<>();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			logger.debug("collection name" + string);
			servicesCollection.put(string, serviceshelfDatabase.getCollection(string));
		}
		//servicesCollection = serviceshelfDatabase.getCollection(collectionName);
	}

	/**
	 * Destruction of the component (used by Spring)
	 */
	public void teardown() {
		logger.debug("ServiceDao teardown");
	}
	
	/**
	 * Wrap the Service creation
	 * @param domain
	 * @param serviceToCreate
	 * @return
	 */
	public Optional<Service> create(String domain, final Service serviceToCreate) {
		checkNotNull(serviceToCreate, "Argument[serviceToCreate] must not be null");

		Service service = new ServiceBuilder(serviceToCreate.getServicename()).build(serviceToCreate);
		service.setServicename(service.getServicename());
		service.set_id(service.getServicename());
		try {
			if (!servicesCollection.containsKey(domain+"_Services")){
				DB serviceshelfDatabase = mongoClient.getDB(dbName);
				servicesCollection.put(domain+"_Services", serviceshelfDatabase.getCollection(domain+"_Services"));
			}
			servicesCollection.get(domain+"_Services").insert(AppUtils.toDBObject(service));
			logger.info("Added new book{}", serviceToCreate);
			return Optional.of(serviceToCreate);
		} catch (MongoException.DuplicateKey e) {
			logger.info("Book with isbn[{}] already exists", serviceToCreate.getServicename());
			return Optional.empty(); // book already exists
		}
	}

	/**
	 * Wrap the get All Services
	 * @param domain
	 * @return
	 */
	public List<Service> readAll(String domain) {
		final List<Service> services = new ArrayList<>();
		if (!servicesCollection.containsKey(domain+"_Services")){
			DB serviceshelfDatabase = mongoClient.getDB(dbName);
			servicesCollection.put(domain+"_Services", serviceshelfDatabase.getCollection(domain+"_Services"));
		}
		try (DBCursor cursor = servicesCollection.get(domain+"_Services").find()) {
			while (cursor.hasNext()) {
				DBObject dbObject = cursor.next();
				services.add((Service) AppUtils.fromDBObject(dbObject, Service.class));
			}
		}
		logger.info("Retrieved [{}] books", services.size());
		return services;
	}

	/**
	 * Wrap the get service for a specific service
	 * @param domain
	 * @param servicename
	 * @return
	 */
	public Optional<Service> readByServicename(String domain, final String servicename) {
		logger.info("readByServiceName " + domain + " " + servicename);
		checkNotNull(servicename, "Argument[servicename] must not be null");
		if (!servicesCollection.containsKey(domain+"_Services")){
			DB serviceshelfDatabase = mongoClient.getDB(dbName);
			servicesCollection.put(domain+"_Services", serviceshelfDatabase.getCollection(domain+"_Services"));
		}
		DBObject query = new BasicDBObject("_id", servicename);
		DBObject dbObject = servicesCollection.get(domain+"_Services").findOne(query);

		if (dbObject != null) {
			Service service = (Service) AppUtils.fromDBObject(dbObject, Service.class);
			logger.info("Retrieved book for servicename[{}]:{}", servicename, service);
			return Optional.of(service);
		}
		logger.info("Book with servicename[{}] does not exist", servicename);
		return Optional.empty();
	}

	/**
	 * Update service information
	 * @param domain
	 * @param serviceToUpdate
	 * @return
	 */
	public Optional<Service> update(String domain, final Service serviceToUpdate) {
		checkNotNull(serviceToUpdate, "Argument[bookToUpdate] must not be null");
		if (!servicesCollection.containsKey(domain+"_Services")){
			DB serviceshelfDatabase = mongoClient.getDB(dbName);
			servicesCollection.put(domain+"_Services", serviceshelfDatabase.getCollection(domain+"_Services"));
		}
		DBObject query = new BasicDBObject("_id", serviceToUpdate.getServicename());
		serviceToUpdate.set_id(serviceToUpdate.getServicename());
		WriteResult result = servicesCollection.get(domain+"_Services").update(
				query, AppUtils.toDBObject(serviceToUpdate));

		if (result.getN() == 1) {
			logger.info("Updated book with servicename[{}]", serviceToUpdate.getServicename());
			return Optional.of(serviceToUpdate);
		}
		logger.info("Book with servicename[{}] does not exist");
		return Optional.empty(); // book does not exist
	}

	/**
	 * Delete service from the base
	 * @param domain
	 * @param serviceToDelete
	 * @return
	 */
	public boolean delete(String domain, final Service serviceToDelete) {
		checkNotNull(serviceToDelete, "Argument[bookToDelete] must not be null");
		if (!servicesCollection.containsKey(domain+"_Services")){
			DB serviceshelfDatabase = mongoClient.getDB(dbName);
			servicesCollection.put(domain+"_Services", serviceshelfDatabase.getCollection(domain+"_Services"));
		}

		DBObject query = new BasicDBObject("_id", serviceToDelete.getServicename());
		WriteResult result = servicesCollection.get(domain+"_Services").remove(query);

		if (result.getN() == 1) {
			logger.info("Deleted book with servicename[{}]", serviceToDelete.getServicename());
			return true;
		}
		logger.info("Book with servicename[{}] does not exist", serviceToDelete.getServicename());
		return false;
	}

	/**
	 * Set the MongoDB client (used by Spring)
	 * @param mongoClient
	 */
	public void setMongoClient(final MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}

	public void setDbName(final String dbName) {
		this.dbName = dbName;
	}

	public void setCollectionName(final String collectionName) {
	}

	/**
	 * Return all the domains contained into the database
	 * @return
	 */
	public  Set<String> getDomains() {
		// TODO Auto-generated method stub
		return servicesCollection.keySet();
	}
}
