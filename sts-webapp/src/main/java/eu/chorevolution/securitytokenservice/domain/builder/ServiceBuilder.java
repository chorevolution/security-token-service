/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.domain.builder;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;
import eu.chorevolution.securitytokenservice.federationserver.api.Service;

/**
 * @author Frederic Motte
 * UserBuilder Used to convert Database end user to EndUser classes
 */
public class ServiceBuilder implements DomainBuilder<Service> {

	private static final Logger logger = LoggerFactory.getLogger(ServiceBuilder.class);
	
	// required parameters
	private String servicename;

	// optional parameters
	private String serviceaccount;
	private String credentialtype;
	private String credential;

	
	/**
	 * Constructor
	 * @param servicename
	 */
	public ServiceBuilder(final String servicename) {
		this.servicename = servicename;
	}

	/**
	 * Set serviceaccount
	 * @param serviceaccount
	 * @return
	 */
	public ServiceBuilder serviceaccount(final String serviceaccount) {
		this.serviceaccount = serviceaccount;
		return this;
	}

	/**
	 * Set serviceaccount
	 * @param credentialtype
	 * @return
	 */
	public ServiceBuilder credentialtype(final String credentialtype) {
		this.credentialtype = credentialtype;
		return this;
	}

	/**
	 * Set credential
	 * @param credential
	 * @return
	 */
	public ServiceBuilder credential(final String credential) {
		this.credential = credential;
		return this;
	}


	@Override
	public Service build() {
		Service user = new Service();

		user.setServicename(servicename);
		user.setServiceaccount(serviceaccount);
		user.setCredentialtype(credentialtype);
		user.setCredential(credential);
		return user;
	}

	@Override
	public Service build(final Service service) {
		Service copy = new Service();

		copy.setServicename(this.servicename);
		copy.setServiceaccount(service.getServiceaccount());
		copy.setCredentialtype(service.getCredentialtype());
		copy.setCredential(service.getCredential());
		return copy;
	}
}
