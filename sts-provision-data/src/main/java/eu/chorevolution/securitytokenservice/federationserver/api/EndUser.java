/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

/**
 * EndUser repesente the user into the rest request
 */
@XmlRootElement(name = "enduser")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {
    "username", "password", "active", "choreographies", "groups", "serviceCredentials", "attributes" })
public class EndUser implements Serializable, Comparable<EndUser> {

    private static final long serialVersionUID = -3901863997664130719L;

    // CHECKSTYLE:OFF
    private String _id;
    // CHECKSTYLE:ON

    private String username;

    private String password;

    private boolean active = true;

    private final Set<String> choreographies = new HashSet<>();

    private final Set<String> groups = new HashSet<>();

    private final Map<String, Map.Entry<String, String>> serviceCredentials = new HashMap<>();

    private final Map<String, Set<String>> attributes = new HashMap<>();

    // CHECKSTYLE:OFF
    @XmlTransient
    public String get_id() {
        return _id;
    }

    public void set_id(final String _id) {
        this._id = _id;
    }
    // CHECKSTYLE:ON

    @XmlElement(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    @XmlElement(name = "password")
    public String getPassword() {
        return password;
    }

    @XmlElement(name = "active")
    public boolean isActive() {
        return active;
    }

    public void setActive(final boolean active) {
        this.active = active;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @XmlElement(name = "groups")
    public Set<String> getGroups() {
        return groups;
    }

    @XmlElement(name = "choreographies")
    public Set<String> getChoreographies() {
        return choreographies;
    }

    @XmlElement(name = "serviceCredentials")
    public Map<String, Map.Entry<String, String>> getServiceCredentials() {
        return serviceCredentials;
    }

    @XmlElement(name = "attributes")
    public Map<String, Set<String>> getAttributes() {
        return attributes;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof EndUser)) {
            return false;
        }
        EndUser that = (EndUser) o;
        return Objects.equal(this.username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username);
    }

    @SuppressWarnings("deprecation")
    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("usernameee", username)
                .add("password", password)
                .add("active", active)
                .add("choreographies", choreographies)
                .add("groups", groups)
                .add("serviceCredentials", serviceCredentials)
                .add("attributes", attributes)
                .toString();
    }

    @Override
    public int compareTo(final EndUser that) {
        return ComparisonChain.start()
                .compare(this.username, that.username)
                .result();
    }
}
