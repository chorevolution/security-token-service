/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;

import eu.chorevolution.securitytokenservice.domain.builder.UserBuilder;
import eu.chorevolution.securitytokenservice.util.AppUtils;

/**
 * @author Frederic Motte
 * UsersDao Wrap the usage of the MongoDB database for the enduser information
 */
public class UsersDao {
	private static final Logger logger = LoggerFactory.getLogger(UsersDao.class);

	private MongoClient mongoClient;
	private String dbName;
	private Map<String, DBCollection> usersCollection;

	public UsersDao() {
	}

	/**
	 * Initialization of the component (used by Spring)
	 * @throws UnknownHostException
	 */
	public void init() throws UnknownHostException {
		logger.debug("UserDao init");
		DB usershelfDatabase = mongoClient.getDB(dbName);
		Set<String> list = usershelfDatabase.getCollectionNames();
		usersCollection = new HashMap<>();
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			logger.debug("collection name" + string);
			usersCollection.put(string, usershelfDatabase.getCollection(string));
		}
		//usersCollection = usershelfDatabase.getCollection(collectionName);
	}

	/**
	 * Destruction of the component (used by Spring)
	 */
	public void teardown() {
		logger.debug("UserDao teardown");
	}
	
	/**
	 * Wrap the User creation
	 * @param domain
	 * @param userToCreate
	 * @return
	 */
	public Optional<EndUser> create(String domain, final EndUser userToCreate) {
		checkNotNull(userToCreate, "Argument[userToCreate] must not be null");

		EndUser user = new UserBuilder(userToCreate.getUsername()).build(userToCreate);
		user.setUsername(user.getUsername());
		user.set_id(user.getUsername());
		try {
			if (!usersCollection.containsKey(domain)){
				DB usershelfDatabase = mongoClient.getDB(dbName);
				usersCollection.put(domain, usershelfDatabase.getCollection(domain));
			}
			usersCollection.get(domain).insert(AppUtils.toDBObject(user));
			logger.info("Added new book{}", userToCreate);
			return Optional.of(userToCreate);
		} catch (MongoException.DuplicateKey e) {
			logger.info("Book with isbn[{}] already exists", userToCreate.getUsername());
			return Optional.empty(); // book already exists
		}
	}

	/**
	 * Wrap the get All Users
	 * @param domain
	 * @return
	 */
	public List<EndUser> readAll(String domain) {
		final List<EndUser> users = new ArrayList<>();
		if (!usersCollection.containsKey(domain)){
			DB usershelfDatabase = mongoClient.getDB(dbName);
			usersCollection.put(domain, usershelfDatabase.getCollection(domain));
		}
		try (DBCursor cursor = usersCollection.get(domain).find()) {
			while (cursor.hasNext()) {
				DBObject dbObject = cursor.next();
				users.add((EndUser) AppUtils.fromDBObject(dbObject, EndUser.class));
			}
		}
		logger.info("Retrieved [{}] books", users.size());
		return users;
	}

	/**
	 * Wrap the get user for a specific user
	 * @param domain
	 * @param username
	 * @return
	 */
	public Optional<EndUser> readByUsername(String domain, final String username) {
		logger.info("readByUserName " + domain + " " + username);
		checkNotNull(username, "Argument[username] must not be null");
		if (!usersCollection.containsKey(domain)){
			DB usershelfDatabase = mongoClient.getDB(dbName);
			usersCollection.put(domain, usershelfDatabase.getCollection(domain));
		}
		DBObject query = new BasicDBObject("_id", username);
		DBObject dbObject = usersCollection.get(domain).findOne(query);

		if (dbObject != null) {
			EndUser user = (EndUser) AppUtils.fromDBObject(dbObject, EndUser.class);
			logger.info("Retrieved book for username[{}]:{}", username, user);
			return Optional.of(user);
		}
		logger.info("Book with username[{}] does not exist", username);
		return Optional.empty();
	}

	/**
	 * Update user information
	 * @param domain
	 * @param userToUpdate
	 * @return
	 */
	public Optional<EndUser> update(String domain, final EndUser userToUpdate) {
		checkNotNull(userToUpdate, "Argument[bookToUpdate] must not be null");
		if (!usersCollection.containsKey(domain)){
			DB usershelfDatabase = mongoClient.getDB(dbName);
			usersCollection.put(domain, usershelfDatabase.getCollection(domain));
		}
		DBObject query = new BasicDBObject("_id", userToUpdate.getUsername());
		userToUpdate.set_id(userToUpdate.getUsername());
		WriteResult result = usersCollection.get(domain).update(
				query, AppUtils.toDBObject(userToUpdate));

		if (result.getN() == 1) {
			logger.info("Updated book with username[{}]", userToUpdate.getUsername());
			return Optional.of(userToUpdate);
		}
		logger.info("Book with username[{}] does not exist");
		return Optional.empty(); // book does not exist
	}

	/**
	 * Delete user from the base
	 * @param domain
	 * @param userToDelete
	 * @return
	 */
	public boolean delete(String domain, final EndUser userToDelete) {
		checkNotNull(userToDelete, "Argument[bookToDelete] must not be null");
		if (!usersCollection.containsKey(domain)){
			DB usershelfDatabase = mongoClient.getDB(dbName);
			usersCollection.put(domain, usershelfDatabase.getCollection(domain));
		}

		DBObject query = new BasicDBObject("_id", userToDelete.getUsername());
		WriteResult result = usersCollection.get(domain).remove(query);

		if (result.getN() == 1) {
			logger.info("Deleted book with username[{}]", userToDelete.getUsername());
			return true;
		}
		logger.info("Book with username[{}] does not exist", userToDelete.getUsername());
		return false;
	}

	/**
	 * Set the MongoDB client (used by Spring)
	 * @param mongoClient
	 */
	public void setMongoClient(final MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}

	public void setDbName(final String dbName) {
		this.dbName = dbName;
	}

	public void setCollectionName(final String collectionName) {
	}

	/**
	 * Return all the domains contained into the database
	 * @return
	 */
	public  Set<String> getDomains() {
		// TODO Auto-generated method stub
		return usersCollection.keySet();
	}
}
