/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.domain.builder;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;

/**
 * @author Frederic Motte
 * UserBuilder Used to convert Database end user to EndUser classes
 */
public class UserBuilder implements DomainBuilder<EndUser> {

	private static final Logger logger = LoggerFactory.getLogger(UserBuilder.class);
	
	// required parameters
	private String username;

	// optional parameters
	private String password;
	private Boolean active = true;
	private Set<String> groups = Collections.emptySet();
	private Set<String> choreographies = Collections.emptySet();
        private Map<String, Map.Entry<String, String>> serviceCredentials = new HashMap<>();
	private Map<String, Set<String>> attributes = new HashMap<>();
	
	/**
	 * Constructor
	 * @param username
	 */
	public UserBuilder(final String username) {
		this.username = username;
	}

	/**
	 * Set Password
	 * @param password
	 * @return
	 */
	public UserBuilder password(final String password) {
		this.password = password;
		return this;
	}

	/**
	 * Set active
	 * @param active
	 * @return
	 */
	public UserBuilder active(final Boolean active) {
		this.active = active;
		return this;
	}

	/**
	 * Set choreographies
	 * @param choreographies
	 * @return
	 */
	public UserBuilder choreographies(final Set<String> choreographies) {
		this.choreographies = choreographies;
		return this;
	}

	/**
	 * Set groups
	 * @param groups
	 * @return
	 */
	public UserBuilder groups(final Set<String> groups) {
		this.groups = groups;
		return this;
	}


	public UserBuilder serviceCredentials(final Map<String, Map.Entry<String,String>> serviceCredentials) {
		this.serviceCredentials = serviceCredentials;
		return this;
	}	
	
	/**
	 * Set Attributes
	 * @param attributes
	 * @return
	 */
	public UserBuilder attributes(final Map<String, Set<String>> attributes) {
		this.attributes = attributes;
		return this;
	}	
	
	@Override
	public EndUser build() {
		EndUser user = new EndUser();

		user.setUsername(username);
		user.setPassword(password);
		user.setActive(active);
		user.getChoreographies().addAll(choreographies);
		user.getGroups().addAll(groups);
		user.getServiceCredentials().putAll(serviceCredentials);
		user.getAttributes().putAll(attributes);
		return user;
	}

	@Override
	public EndUser build(final EndUser user) {
		EndUser copy = new EndUser();

		copy.setUsername(this.username);
		copy.setPassword(user.getPassword());
		copy.setActive(user.isActive());
		copy.getChoreographies().addAll(user.getChoreographies());
		copy.getGroups().addAll(user.getGroups());
		copy.getServiceCredentials().putAll(user.getServiceCredentials());
		copy.getAttributes().putAll(user.getAttributes());
		return copy;
	}
}
