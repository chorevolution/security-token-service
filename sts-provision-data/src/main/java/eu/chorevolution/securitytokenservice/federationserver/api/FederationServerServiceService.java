/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface FederationServerServiceService {

    String BOOK_NOT_FOUND_MSG = "Book with ISBN[%s] does not exist";

    String BOOK_EXISTS_MSG = "Book with ISBN[%s] already exists";

    /**
     * Return all the services of a domain
     *
     * @param domain
     * @return
     */
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    Response list(@PathParam("domain") String domain);

    /**
     * Create new service into the domain
     *
     * @param domain
     * @param service
     * @return
     */
    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    Response create(@PathParam("domain") String domain, Service service);

    /**
     * Get information for the specified service
     *
     * @param domain
     * @param servicename
     * @return
     */
    @GET
    @Path("{servicename}")
    @Produces({ MediaType.APPLICATION_JSON })
    Response read(@PathParam("domain") String domain, @PathParam("servicename") String servicename);

    /**
     * Modify service information
     *
     * @param domain
     * @param servicename
     * @param service
     * @return
     */
    @PUT
    @Path("{servicename}")
    @Consumes({ MediaType.APPLICATION_JSON })
    Response update(
            @PathParam("domain") String domain,
            @PathParam("servicename") String servicename,
            Service service);

    /**
     * Delete service from the domain
     *
     * @param domain
     * @param servicename
     * @return
     */
    @DELETE
    @Path("{servicename}")
    Response delete(@PathParam("domain") String domain, @PathParam("servicename") String servicename);

}
