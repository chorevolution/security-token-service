/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.inout;

import javax.xml.bind.JAXBElement;
import org.apache.cxf.sts.QNameConstants;
import org.apache.cxf.sts.STSConstants;
import org.apache.cxf.sts.token.provider.TokenProviderResponse;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseType;
import org.apache.cxf.ws.security.sts.provider.model.RequestedSecurityTokenType;
import org.apache.cxf.ws.security.sts.provider.model.StatusType;
import org.apache.cxf.ws.security.sts.provider.model.secext.UsernameTokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Build the response returned to the STS client
 * @author Frederic Motte
 *
 */
public class ResponseBuilder {

	private static final Logger logger = LoggerFactory.getLogger(ResponseBuilder.class);

	/**
	 * Build response for validate request
	 * @param tokenValid
	 * @param tokenType
	 * @return
	 */
	public RequestSecurityTokenResponseType validateResponse(Boolean tokenValid, String tokenType) {
		logger.info("Build validate response with response " + tokenValid);
        RequestSecurityTokenResponseType response = 
                QNameConstants.WS_TRUST_FACTORY.createRequestSecurityTokenResponseType();
        
        if (tokenValid || STSConstants.STATUS.equals(tokenType)) {
            JAXBElement<String> jaxbTokenType = 
                QNameConstants.WS_TRUST_FACTORY.createTokenType(tokenType);
            response.getAny().add(jaxbTokenType);
        }
        
        StatusType statusType = QNameConstants.WS_TRUST_FACTORY.createStatusType();
        if (tokenValid) {
            statusType.setCode(STSConstants.VALID_CODE);
            statusType.setReason(STSConstants.VALID_REASON);
        } else {
            statusType.setCode(STSConstants.INVALID_CODE);
            statusType.setReason(STSConstants.INVALID_REASON);
        }
        JAXBElement<StatusType> status = QNameConstants.WS_TRUST_FACTORY.createStatus(statusType);
        response.getAny().add(status);
		// TODO Auto-generated method stub
		return response;
	}

	public RequestSecurityTokenResponseType issueResponse(boolean tokenValid, String tokenType, JAXBElement<UsernameTokenType> token) {


		logger.info("Build issue response with response " + tokenType);
        RequestSecurityTokenResponseType response = 
                QNameConstants.WS_TRUST_FACTORY.createRequestSecurityTokenResponseType();
        
        if (tokenValid) {
            JAXBElement<String> jaxbTokenType = 
                QNameConstants.WS_TRUST_FACTORY.createTokenType(tokenType);
            response.getAny().add(jaxbTokenType);
        }
        

        // RequestedSecurityToken
        RequestedSecurityTokenType requestedTokenType = 
            QNameConstants.WS_TRUST_FACTORY.createRequestedSecurityTokenType();
        JAXBElement<RequestedSecurityTokenType> requestedToken = 
            QNameConstants.WS_TRUST_FACTORY.createRequestedSecurityToken(requestedTokenType);

        requestedTokenType.setAny(token);
       
        response.getAny().add(requestedToken);
		return response;
	}
}
