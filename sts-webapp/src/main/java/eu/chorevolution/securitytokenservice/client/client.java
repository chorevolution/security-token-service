/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.client;

import javax.xml.bind.JAXBElement;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.sts.QNameConstants;
import org.apache.cxf.sts.STSConstants;
import org.apache.cxf.sts.operation.TokenRequestCollectionOperation;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;
import org.apache.cxf.ws.security.sts.provider.model.StatusType;
import org.apache.cxf.ws.security.sts.provider.model.ValidateTargetType;
import org.apache.cxf.ws.security.sts.provider.model.secext.AttributedString;
import org.apache.cxf.ws.security.sts.provider.model.secext.PasswordString;
import org.apache.cxf.ws.security.sts.provider.model.secext.UsernameTokenType;
import org.apache.ws.security.WSConstants;
import org.opensaml.ws.wstrust.WSTrustConstants;
import org.apache.ws.security.WSConstants;
import eu.chorevolution.securitytokenservice.SecurityTokenService;
import eu.chorevolution.securitytokenservice.SecurityTokenServiceConstants;

import javax.xml.namespace.QName;
public class client {

	private static final QName QNAME_WST_STATUS = 
			QNameConstants.WS_TRUST_FACTORY.createStatus(null).getName();

	public static void main(String[] args) {

//		//create WebService client proxy factory
//		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
//		//register WebService interface
//		factory.setServiceClass(SecurityTokenService.class);
//		//set webservice publish address to factory.
//		factory.setAddress("http://localhost:8080/SecurityTokenService/services/securitytokenservice");
//		SecurityTokenService iHelloWorld = (SecurityTokenService) factory.create();
//
//
//
//
//		{
//
//
//
//			System.out.println("invoke webservice validate token...");
//			RequestSecurityTokenType request = new RequestSecurityTokenType();
//
//			JAXBElement<String> requestType = 
//					new JAXBElement<String>(
//							QNameConstants.REQUEST_TYPE, String.class, TokenRequestCollectionOperation.WSTRUST_REQUESTTYPE_BATCH_VALIDATE
//							);
//			request.getAny().add(requestType);
//
//			JAXBElement<UsernameTokenType> usernameTokenType = createUsernameToken("alice", "clarinet", "domain1");
//			ValidateTargetType validateTarget = new ValidateTargetType();
//			validateTarget.setAny(usernameTokenType);
//
//			JAXBElement<ValidateTargetType> validateTargetType = 
//					new JAXBElement<ValidateTargetType>(
//							QNameConstants.VALIDATE_TARGET, ValidateTargetType.class, validateTarget
//							);
//			request.getAny().add(validateTargetType);
//			RequestSecurityTokenResponseType responce = iHelloWorld.validate(request);
//			System.out.println("message context is:" + responce);
//
//
//
//			for (Object requestObject : responce.getAny()) {
//				// JAXB types
//				if (requestObject instanceof JAXBElement<?>) {
//					JAXBElement<?> jaxbElement = (JAXBElement<?>) requestObject;
//					System.out.println("Found " + jaxbElement.getName() + ": " + jaxbElement.getValue());
//
//					if (QNAME_WST_STATUS.equals(jaxbElement.getName())) {
//						StatusType status = (StatusType)jaxbElement.getValue();
//						if (STSConstants.VALID_CODE.equals(status.getCode())) {
//							System.out.println("is valid");
//						}
//						else
//						{
//							System.out.println("is not valid");
//						}
//					}
//				}
//			}
//
//		}
//		{
//
//			System.out.println("invoke webservice issue token...");
//			RequestSecurityTokenType request = new RequestSecurityTokenType();
//			JAXBElement<String> tokenType = 
//					new JAXBElement<String>(
//							QNameConstants.TOKEN_TYPE, String.class, WSConstants.WSS_USERNAME_TOKEN_VALUE_TYPE
//							);
//			request.getAny().add(tokenType);
//
//
//			JAXBElement<String> domainElement = 
//					new JAXBElement<String>(
//							SecurityTokenServiceConstants.SECURITY_DOMAIN, String.class, "domain2"
//							);
//			request.getAny().add(domainElement);
//			
//			JAXBElement<String> requestType = 
//					new JAXBElement<String>(
//							QNameConstants.REQUEST_TYPE, String.class, TokenRequestCollectionOperation.WSTRUST_REQUESTTYPE_BATCH_ISSUE
//							);
//			request.getAny().add(requestType);
//
//			JAXBElement<UsernameTokenType> usernameTokenType = createUsernameToken("alice", "clarinet", "domain1");
//			ValidateTargetType validateTarget = new ValidateTargetType();
//			validateTarget.setAny(usernameTokenType);
//
//			JAXBElement<ValidateTargetType> validateTargetType = 
//					new JAXBElement<ValidateTargetType>(
//							QNameConstants.VALIDATE_TARGET, ValidateTargetType.class, validateTarget
//							);
//			request.getAny().add(validateTargetType);
//			request.setContext("domain");
//			RequestSecurityTokenResponseType responce = iHelloWorld.validate(request);
//			System.out.println("message context is:" + responce);
//
//
//
//			for (Object requestObject : responce.getAny()) {
//				// JAXB types
//				if (requestObject instanceof JAXBElement<?>) {
//					JAXBElement<?> jaxbElement = (JAXBElement<?>) requestObject;
//					System.out.println("Found " + jaxbElement.getName() + ": " + jaxbElement.getValue());
//
//					if (QNAME_WST_STATUS.equals(jaxbElement.getName())) {
//						StatusType status = (StatusType)jaxbElement.getValue();
//						if (STSConstants.VALID_CODE.equals(status.getCode())) {
//							System.out.println("is valid");
//						}
//						else
//						{
//							System.out.println("is not valid");
//						}
//					}
//				}
//			}
//
//		}








		System.exit(0);
	}

//	private static JAXBElement<UsernameTokenType> createUsernameToken(String name, String password, String domain) {
//		UsernameTokenType usernameToken = new UsernameTokenType();
//		AttributedString username = new AttributedString();
//		username.setValue(name);
//		usernameToken.setUsername(username);
//
//		// Add a password
//		PasswordString passwordString = new PasswordString();
//		passwordString.setValue(password);
//		passwordString.setType(WSConstants.PASSWORD_TEXT);
//		JAXBElement<PasswordString> passwordType = 
//				new JAXBElement<PasswordString>(
//						QNameConstants.PASSWORD, PasswordString.class, passwordString
//						);
//		usernameToken.getAny().add(passwordType);
//		
//		JAXBElement<String> domainElement = 
//				new JAXBElement<String>(
//						SecurityTokenServiceConstants.SECURITY_DOMAIN, String.class, domain
//						);
//		usernameToken.getAny().add(domainElement);
//		
//		
//		JAXBElement<UsernameTokenType> tokenType = 
//				new JAXBElement<UsernameTokenType>(
//						QNameConstants.USERNAME_TOKEN, UsernameTokenType.class, usernameToken
//						);
//
//		
//		
//		
//		
//		return tokenType;
//	}
}
