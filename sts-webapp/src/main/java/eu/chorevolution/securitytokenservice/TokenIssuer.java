/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.bind.JAXBElement;

import org.apache.cxf.sts.QNameConstants;
import org.apache.cxf.ws.security.sts.provider.model.secext.AttributedString;
import org.apache.cxf.ws.security.sts.provider.model.secext.PasswordString;
import org.apache.cxf.ws.security.sts.provider.model.secext.UsernameTokenType;
import org.apache.ws.security.WSConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securitytokenservice.datamodel.Token;
import eu.chorevolution.securitytokenservice.datamodel.UserNameToken;
import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;
import eu.chorevolution.securitytokenservice.federationserver.api.Service;
import eu.chorevolution.securitytokenservice.federationserver.api.ServicesDao;
import eu.chorevolution.securitytokenservice.federationserver.api.UsersDao;
import eu.chorevolution.securitytokenservice.inout.RsttContent;
import eu.chorevolution.securitytokenservice.security.Encryptor;

public class TokenIssuer {

	private static final Logger logger = LoggerFactory.getLogger(TokenIssuer.class);
	private Encryptor masterPasswordEncryptor = null;
	private Encryptor applicationPasswordEncryptor = null;
	
	private UsersDao userDao;
	private ServicesDao serviceDao;
	public TokenIssuer(UsersDao userDao, ServicesDao serviceDao, String masterPasswordKey, String applicationKeyPassword) {
		this.serviceDao = serviceDao;
		this.userDao = userDao;
		masterPasswordEncryptor = new Encryptor(masterPasswordKey);
		applicationPasswordEncryptor = new Encryptor(applicationKeyPassword);

	}


	/**
	 * Vaalidate the Token received regarding the user database
	 * @param result result
	 * @param token token
	 * @return UsernameTokenType
	 */
	public JAXBElement<UsernameTokenType> issue(RsttContent result, Token token) {
		logger.info("issue token");
		Boolean validation = false;
		JAXBElement<UsernameTokenType>  issuedToken = null;

		if (token instanceof UserNameToken) {
			logger.info("The token is a UserNameToken with the following parameter");
			UserNameToken new_name = (UserNameToken) token;

			logger.info("	name " + new_name.getUsername());
			logger.info("	password " + new_name.getPassword());
			logger.info("	passwordtype " + new_name.getPasswordType());
			logger.info("   domain " + new_name.getDomain());

			//			Set<String> domainList = null;
			//			if (new_name.getDomain() == null)
			//			{
			//				domainList = userDao.getDomains();
			//			}
			//			else
			//			{
			//				domainList = new TreeSet<String>();
			//				domainList.add(new_name.getDomain());
			//			}

			//			// find in all the domains is a valid user is present
			//			for (Iterator iterator = domainList.iterator(); iterator.hasNext();) {
			//				String domain = (String) iterator.next();
			String domain = new_name.getDomain();
			logger.info("Search in domain  " + domain);
			// find all users for this name
			if (result.getConsumerType().equalsIgnoreCase("user")){

				Optional<EndUser> res = userDao.readByUsername(domain, new_name.getUsername());
				if (res.isPresent()){
					// if the two password are equals, then the user is valid
					
					try {
						String decodedPassword = masterPasswordEncryptor.decode(res.get().getPassword());
						logger.info("Compare password " + decodedPassword + " " + new_name.getPassword());
						if (decodedPassword.equals(new_name.getPassword()))
						{
							logger.info("The UserNameToken is valid");
							validation = true;
						}
					} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
							| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			} else if (result.getConsumerType().equalsIgnoreCase("service")){
				Optional<Service> res = serviceDao.readByServicename(domain, new_name.getUsername());
				if (res.isPresent()){
					// if the two password are equals, then the user is valid
					try {
						String decodedPassword = masterPasswordEncryptor.decode(res.get().getCredential());
						logger.info("Compare password " + decodedPassword + " " + new_name.getPassword());
						if (decodedPassword.equals(new_name.getPassword()))
						{
							logger.info("The UserNameToken is valid");
							validation = true;

						}
					} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
							| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} else {
				//TODO throw an exception
			}


			if (validation){
				logger.info("The UserNameToken is valid : check the result.getProviderType() : " +result.getProviderType() );
				if (result.getProviderType().equalsIgnoreCase("user")){

					Optional<EndUser> res = userDao.readByUsername(domain, new_name.getUsername());
					if (res.isPresent()){						
						if (res.get().getServiceCredentials()!=null)
						{
							if (res.get().getServiceCredentials().get(result.getProviderName())!=null)
							{
								Map.Entry<String, String> mapcred = res.get().getServiceCredentials().get(result.getProviderName());
								// if the two password are equals, then the user is valid
								try {
									String decodedPassword = applicationPasswordEncryptor.decode(mapcred.getValue());
									issuedToken = createUsernameToken(mapcred.getKey(), decodedPassword , WSConstants.PASSWORD_TEXT);
								} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
										| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						}
					}
				} else if (result.getProviderType().equalsIgnoreCase("service")){
					Optional<Service> service = serviceDao.readByServicename(result.getDomain(), result.getProviderName());
					if (service.isPresent()){
						try {
							String decodedPassword = applicationPasswordEncryptor.decode(service.get().getCredential());
							issuedToken = createUsernameToken(service.get().getServiceaccount(), decodedPassword, WSConstants.PASSWORD_TEXT);
						} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
								| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else{
						//TODO : throw an exception
					}
				}

			}

		}



		return issuedToken;
	}


	private static JAXBElement<UsernameTokenType> createUsernameToken(String name, String password, String passwordkind) {
		UsernameTokenType usernameToken = new UsernameTokenType();
		AttributedString username = new AttributedString();
		username.setValue(name);
		usernameToken.setUsername(username);

		// Add a password
		PasswordString passwordString = new PasswordString();
		passwordString.setValue(password);
		//		passwordString.setType(WSConstants.PASSWORD_TEXT);
		passwordString.setType(passwordkind);

		JAXBElement<PasswordString> passwordType = 
				new JAXBElement<PasswordString>(
						QNameConstants.PASSWORD, PasswordString.class, passwordString
						);
		usernameToken.getAny().add(passwordType);

		JAXBElement<UsernameTokenType> tokenType = 
				new JAXBElement<UsernameTokenType>(
						QNameConstants.USERNAME_TOKEN, UsernameTokenType.class, usernameToken
						);

		return tokenType;
	}

}
