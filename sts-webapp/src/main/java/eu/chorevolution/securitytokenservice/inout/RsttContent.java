/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.inout;

import org.apache.cxf.ws.security.sts.provider.model.ValidateTargetType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securitytokenservice.datamodel.Token;

/**
 * simple representation of the RequestSecurityTokenType
 * @author Frederic Motte
 *
 */
public class RsttContent {

	private static final Logger logger = LoggerFactory.getLogger(RsttContent.class);

	private String tokenType;
	private String RequestType;
	private String domain;
	
	private ValidateTargetType target;
	private Token token;
	
	private String consumerType;
	private String providerType;
	private String providerName;
	
	
	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getRequestType() {
		return RequestType;
	}

	public void setRequestType(String requestType) {
		RequestType = requestType;
	}

	public ValidateTargetType getTarget() {
		return target;
	}

	public void setTarget(ValidateTargetType target) {
		this.target = target;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getConsumerType() {
		return consumerType;
	}

	public void setConsumerType(String consumerType) {
		this.consumerType = consumerType;
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	
	
}
