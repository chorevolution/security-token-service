/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securitytokenservice.datamodel.Token;
import eu.chorevolution.securitytokenservice.datamodel.UserNameToken;
import eu.chorevolution.securitytokenservice.federationserver.api.EndUser;
import eu.chorevolution.securitytokenservice.federationserver.api.Service;
import eu.chorevolution.securitytokenservice.federationserver.api.ServicesDao;
import eu.chorevolution.securitytokenservice.federationserver.api.UsersDao;
import eu.chorevolution.securitytokenservice.inout.RsttContent;
import eu.chorevolution.securitytokenservice.security.Encryptor;


/**
 * @author Frederic Motte
 * Validator for token
 */
public class TokenValidator {

	private static final Logger logger = LoggerFactory.getLogger(TokenValidator.class);
	private Encryptor masterPasswordEncryptor = null;
	
	private UsersDao userDao;
	private ServicesDao serviceDao;
	public TokenValidator(UsersDao userDao, ServicesDao serviceDao, String masterPasswordKey) {
		// TODO Auto-generated constructor stub
		this.serviceDao = serviceDao;
		this.userDao = userDao;
		masterPasswordEncryptor = new Encryptor(masterPasswordKey);
	}


	/**
	 * Vaalidate the Token received regarding the user database
	 * @param result result
	 * @param token token
	 * @return boolean
	 */
	public boolean validate(RsttContent result, Token token) {

		logger.info("Validate token");
		Boolean validation = false;

		if (token instanceof UserNameToken) {
			logger.info("The token is a UserNameToken with the following parameter");
			UserNameToken new_name = (UserNameToken) token;

			logger.info("	name " + new_name.getUsername());
			logger.info("	password " + new_name.getPassword());
			logger.info("	passwordtype " + new_name.getPasswordType());
			logger.info("   domain " + new_name.getDomain());

			//			Set<String> domainList = null;
			//			if (new_name.getDomain() == null)
			//			{
			//				domainList = userDao.getDomains();
			//			}
			//			else
			//			{
			//				domainList = new TreeSet<String>();
			//				domainList.add(new_name.getDomain());
			//			}

			//			// find in all the domains is a valid user is present
			//			for (Iterator iterator = domainList.iterator(); iterator.hasNext();) {
			//				String domain = (String) iterator.next();
			String domain = new_name.getDomain();
			logger.info("Search in domain  " + domain);
			logger.info("for consumer type   " + result.getConsumerType());
			// find all users for this name
			if (result.getConsumerType().equalsIgnoreCase("user")){
				Optional<EndUser> res = userDao.readByUsername(domain, new_name.getUsername());
				if (res.isPresent()){
					// if the two password are equals, then the user is valid
					
					try {
						String decodedPassword = masterPasswordEncryptor.decode(res.get().getPassword());
						logger.info("Compare password " + decodedPassword + " " + new_name.getPassword());
						if (decodedPassword.equals(new_name.getPassword()))
						{
							logger.info("The UserNameToken is valid");
							validation = true;
							return validation;
						}
					} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
							| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			} else if (result.getConsumerType().equalsIgnoreCase("service")){
				Optional<Service> res = serviceDao.readByServicename(domain, new_name.getUsername());
				if (res.isPresent()){
					// if the two password are equals, then the user is valid
					try {
						String decodedPassword = masterPasswordEncryptor.decode(res.get().getCredential());
						logger.info("Compare password " + decodedPassword + " " + new_name.getPassword());
						if (decodedPassword.equals(new_name.getPassword()))
						{
							logger.info("The UserNameToken is valid");
							validation = true;
							return validation;
						}
					} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
							| IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			} else {
				//TODO throw an exception
			}
		}
		//		}
		logger.info("The UserNameToken is not valid");
		return validation;
	}
}
