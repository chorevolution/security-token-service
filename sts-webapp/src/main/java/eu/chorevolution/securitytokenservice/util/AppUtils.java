/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class AppUtils {

	private static final Logger logger = LoggerFactory.getLogger(AppUtils.class);

	public static DBObject toDBObject(Object pojo) {

		ObjectMapper om = new ObjectMapper();
		String json;
		try {
			json = om.writeValueAsString(pojo);
			logger.info("json input " + json);
			return (DBObject) JSON.parse(json);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Object fromDBObject(DBObject dbObj, Class clazz) {
		String json = dbObj.toString();
		ObjectMapper om = new ObjectMapper();
		try {
			Object o = om.readValue(json, clazz);
			logger.info("json output" + json);
			return o;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
