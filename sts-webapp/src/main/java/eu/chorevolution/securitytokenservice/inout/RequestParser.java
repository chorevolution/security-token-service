/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.inout;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.cxf.common.jaxb.JAXBContextCache;
import org.apache.cxf.common.jaxb.JAXBContextCache.CachedContextAndSchemas;
import org.apache.cxf.sts.QNameConstants;
import org.apache.cxf.sts.request.ReceivedToken;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;
import org.apache.cxf.ws.security.sts.provider.model.ValidateTargetType;
import org.apache.cxf.ws.security.sts.provider.model.secext.UsernameTokenType;
import org.apache.wss4j.dom.message.token.UsernameToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.wss4j.common.bsp.BSPEnforcer;
import org.w3c.dom.Element;

import eu.chorevolution.securitytokenservice.SecurityTokenServiceConstants;
import eu.chorevolution.securitytokenservice.datamodel.UserNameToken;
import org.apache.cxf.ws.security.sts.provider.model.ObjectFactory;
import org.apache.cxf.helpers.DOMUtils;
import org.w3c.dom.Document;

/**
 * Extract Security information from the request
 * @author Frederic Motte
 * 
 */
public class RequestParser {

	private static final Logger logger = LoggerFactory.getLogger(RequestParser.class);

	/**
	 * Parse request to extract security token information
	 * @param request
	 * @return
	 */
	public RsttContent parseValidateRequest(RequestSecurityTokenType request) {
		// TODO Auto-generated method stub
		logger.debug("Parse RequestSecurityTokenType");
		
		RsttContent res = new RsttContent();

		res.setDomain(request.getOtherAttributes().get(SecurityTokenServiceConstants.SECURITY_ISSUER_DOMAIN));
		res.setConsumerType(request.getOtherAttributes().get(SecurityTokenServiceConstants.SECURITY_CONSUMER_TYPE));
		res.setProviderType(request.getOtherAttributes().get(SecurityTokenServiceConstants.SECURITY_PROVIDER_TYPE));
		res.setProviderName(request.getOtherAttributes().get(SecurityTokenServiceConstants.SECURITY_PROVIDER_NAME));
		
		// for each element of the request security token 
		for (Object requestObject : request.getAny()) {
			// JAXB types
			if (requestObject instanceof JAXBElement<?>) {
				JAXBElement<?> jaxbElement = (JAXBElement<?>) requestObject;
				logger.debug("	Found " + jaxbElement.getName() + ": " + jaxbElement.getValue());

				if (QNameConstants.TOKEN_TYPE.equals(jaxbElement.getName())){
					res.setTokenType(jaxbElement.getValue().toString());
				}
				if (QNameConstants.REQUEST_TYPE.equals(jaxbElement.getName())){
					res.setRequestType(jaxbElement.getValue().toString());
				}
				if (QNameConstants.VALIDATE_TARGET.equals(jaxbElement.getName())) {
					ValidateTargetType validateTargetType = (ValidateTargetType)jaxbElement.getValue();
					ReceivedToken validateTarget = new ReceivedToken(validateTargetType.getAny());
					validateTarget.isUsernameToken();
					UsernameTokenType usernameTokenType = (UsernameTokenType)validateTarget.getToken();
		
					Element usernameTokenElement = null;
					try {
						Set<Class<?>> classes = new HashSet<>();
						classes.add(ObjectFactory.class);
						classes.add(org.apache.cxf.ws.security.sts.provider.model.wstrust14.ObjectFactory.class);
						classes.add(UsernameTokenType.class);

						CachedContextAndSchemas cache = 
								JAXBContextCache.getCachedContextAndSchemas(classes, null, null, null, false);
						JAXBContext jaxbContext = cache.getContext();

						Marshaller marshaller = jaxbContext.createMarshaller();
						Document doc = DOMUtils.createDocument();
						Element rootElement = doc.createElement("root-element");
						JAXBElement<UsernameTokenType> tokenType = 
								new JAXBElement<UsernameTokenType>(
										QNameConstants.USERNAME_TOKEN, UsernameTokenType.class, usernameTokenType
										);
						marshaller.marshal(tokenType, rootElement);
						usernameTokenElement = (Element)rootElement.getFirstChild();


						UsernameToken ut = 
								new UsernameToken(usernameTokenElement, false, 
										new BSPEnforcer());
						
						UserNameToken token = new UserNameToken();
						token.setUsername(ut.getName());
						token.setPasswordType(ut.getPasswordType());
						token.setPassword(ut.getPassword());
						token.setNonce(ut.getNonce());
						token.setId(ut.getID());
						token.setCreated(ut.getCreated());

						token.setDomain(usernameTokenType.getOtherAttributes().get(SecurityTokenServiceConstants.SECURITY_VALIDATION_DOMAIN));
						token.setType(usernameTokenType.getOtherAttributes().get(SecurityTokenServiceConstants.SECURITY_CONSUMER_TYPE));
						res.setToken(token);
						
					} catch (JAXBException | org.apache.wss4j.common.ext.WSSecurityException ex) {
						logger.error("Failed to parse the SecurityRequest : " + ex.getMessage());
						ex.printStackTrace();
						res = null;
					}
				}
			} else
			{
				logger.debug(requestObject.toString());
			}
		}
		return res;
	}
}
