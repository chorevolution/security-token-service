/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.operation;

import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securitytokenservice.inout.RequestParser;
import eu.chorevolution.securitytokenservice.inout.ResponseBuilder;
import eu.chorevolution.securitytokenservice.inout.RsttContent;
import eu.chorevolution.securitytokenservice.TokenValidator;
import eu.chorevolution.securitytokenservice.federationserver.api.ServicesDao;
import eu.chorevolution.securitytokenservice.federationserver.api.UsersDao;

/**
 * Validate operation
 * @author Frederic Motte
 *
 */
public class ValidateOperation {

	private static final Logger logger = LoggerFactory.getLogger(ValidateOperation.class);

	private RequestParser requestparser;
	private TokenValidator tokenValidator;
	private ResponseBuilder responceBuilder;
	private UsersDao userDao;
	private ServicesDao serviceDao;

	public ValidateOperation(UsersDao userDao, ServicesDao serviceDao, String masterPasswordKey) {
		super();
		// TODO Auto-generated constructor stub
		this.requestparser = new RequestParser();
		this.tokenValidator = new TokenValidator(userDao, serviceDao, masterPasswordKey);
		this.responceBuilder = new ResponseBuilder();
	}

	/**
	 * Validate the token contains into the request and return the response
	 * @param request RequestSecurityTokenType
	 * @return RequestSecurityTokenResponseType
	 */
	public RequestSecurityTokenResponseType validate (RequestSecurityTokenType request){
		// parse the request to extract usefull information
		RsttContent result = requestparser.parseValidateRequest(request);
		// validate information
		Boolean tokenValid = tokenValidator.validate (result, result.getToken());
		// build response
		RequestSecurityTokenResponseType response = responceBuilder.validateResponse(tokenValid, result.getTokenType().toString());
		return response;

	}
}
