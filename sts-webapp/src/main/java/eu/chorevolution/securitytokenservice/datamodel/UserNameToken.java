/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.datamodel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.securitytokenservice.datamodel.Token;

/**
 * @author Frederic Motte
 * UserNameToken wrapper classe. A simple view of a UserNameToken
 */
public class UserNameToken extends Token {

	private static final Logger logger = LoggerFactory.getLogger(UserNameToken.class);

	/**
	 * The ID of the Token
	 */
	private String id;
	/**
	 * the username
	 */
	private String username;
	/**
	 * The password
	 */
	private String password;
	/**
	 * The type of password (Clear or encoded)
	 */
	private String passwordType;
	/**
	 * The nonce generated
	 */
	private String nonce;
	/**
	 * The creation date of the token
	 */
	private String created;
	

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the passwordType
	 */
	public String getPasswordType() {
		return passwordType;
	}
	/**
	 * @param passwordType the passwordType to set
	 */
	public void setPasswordType(String passwordType) {
		this.passwordType = passwordType;
	}
	/**
	 * @return the nonce
	 */
	public String getNonce() {
		return nonce;
	}
	/**
	 * @param nonce the nonce to set
	 */
	public void setNonce(String nonce) {
		this.nonce = nonce;
	}
	/**
	 * @return the created
	 */
	public String getCreated() {
		return created;
	}
	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = created;
	}
	/**
	 * @return the logger
	 */
	public static Logger getLogger() {
		return logger;
	}

}
