/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import static java.lang.String.format;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import eu.chorevolution.securitytokenservice.domain.builder.ServiceBuilder;

/**
 * REST operations for end-service management.
 */
@Path("/domains/{domain}/services")
public class FederationServerServiceServiceImpl implements FederationServerServiceService {

	private static final Logger logger = LoggerFactory.getLogger(FederationServerServiceServiceImpl.class);

	private ServicesDao servicesDao;

	@Autowired
	public FederationServerServiceServiceImpl(ServicesDao servicesDao) {
		logger.debug("FederationServerServiceService Constructor");
		this.servicesDao = servicesDao;
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.securitytokenservice.federationserver.api.FederationServerServiceService#list(java.lang.String)
	 */
	@Override
	public Response list(String domain) {
		List<Service> books = new ArrayList<>(servicesDao.readAll(domain));
		GenericEntity<List<Service>> booksEntity = new GenericEntity<List<Service>>(books) {
		};
		return Response.ok(booksEntity).build();
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.securitytokenservice.federationserver.api.FederationServerServiceService#create(java.lang.String, eu.chorevolution.securitytokenservice.federationserver.api.Service)
	 */
	@Override
	public Response create(String domain, Service service) {
		logger.debug("Add a new service into the domain " + domain);
		if (servicesDao.create(domain, service).isPresent()) {
			return Response.status(NO_CONTENT).build();
		}
		return Response.status(BAD_REQUEST)
				.entity(format(BOOK_EXISTS_MSG, service.getServicename()))
				.type(MediaType.TEXT_PLAIN).build();
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.securitytokenservice.federationserver.api.FederationServerServiceService#read(java.lang.String, java.lang.String)
	 */
	@Override
	public Response read(String domain, String servicename) {
		logger.debug("Get Service information for service " + servicename + " into the domain " + domain);
		Optional<Service> optional = servicesDao.readByServicename(domain, servicename);
		if (optional.isPresent()) {
			return Response.ok(optional.get()).build();
		}
		return Response.status(NOT_FOUND).entity(format(BOOK_NOT_FOUND_MSG, servicename)).build();
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.securitytokenservice.federationserver.api.FederationServerServiceService#update(java.lang.String, java.lang.String, eu.chorevolution.securitytokenservice.federationserver.api.Service)
	 */
	@Override
	public Response update(String domain, String servicename, Service service) {
		logger.debug("Modify Service information for service " + servicename + " into the domain " + domain);
		Optional<Service> optional = servicesDao.update(domain, service);
		if (optional.isPresent()) {
			return Response.status(NO_CONTENT).build();
		}
		return Response.status(NOT_FOUND).entity(format(BOOK_NOT_FOUND_MSG, service.getServicename())).build();

	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.securitytokenservice.federationserver.api.FederationServerServiceService#delete(java.lang.String, java.lang.String)
	 */
	@Override
	public Response delete(String domain, String servicename) {
		logger.debug("Delete Service " + servicename + " into the domain " + domain);

		if (servicesDao.delete(domain, new ServiceBuilder(servicename).build())) {
			return Response.status(NO_CONTENT).build();
		}
		return Response.status(NOT_FOUND).entity(format(BOOK_NOT_FOUND_MSG, servicename)).build();
	}

}