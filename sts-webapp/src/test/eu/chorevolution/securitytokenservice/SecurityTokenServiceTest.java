package eu.chorevolution.securitytokenservice;

import static org.junit.Assert.*;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SecurityTokenServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		
		SecurityTokenServiceImpl service = new SecurityTokenServiceImpl();
		
		RequestSecurityTokenType request = null;
		
		request.getAny().add(new JAXBElement<String>(new QName("http://docs.oasis-open.org/ws-sx/ws-trust/200512", "TokenType"), String.class, "http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0" /* or http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV1.1 */ ));

	    request.getAny().add(new JAXBElement<String>(new QName("http://docs.oasis-open.org/ws-sx/ws-trust/200512", "RequestType"), String.class, "http://docs.oasis-open.org/ws-sx/ws-trust/200512/Validate"));

		service.validate(request);
		
		fail("Not yet implemented");
		
		
	}

}
