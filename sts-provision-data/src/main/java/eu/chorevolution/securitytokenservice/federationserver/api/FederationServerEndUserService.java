/*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST operations for end-user management.
 */

public interface FederationServerEndUserService {

    /**
     * Return all the users of a domain.
     *
     * @param domain
     * @return all the users of a domain
     */
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    Response list(@PathParam("domain") String domain);

    /**
     * Create new user into the domain.
     *
     * @param domain
     * @param enduser
     * @return status
     */
    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    Response create(@PathParam("domain") String domain, EndUser enduser);

    /**
     * Get information for the specified user.
     *
     * @param domain
     * @param username
     * @return information for the specified user
     */
    @GET
    @Path("{username}")
    @Produces({ MediaType.APPLICATION_JSON })
    Response read(@PathParam("domain") String domain, @PathParam("username") String username);

    /**
     * Modify user information.
     *
     * @param domain
     * @param username
     * @param enduser
     * @return status
     */
    @PUT
    @Path("{username}")
    @Consumes({ MediaType.APPLICATION_JSON })
    Response update(@PathParam("domain") String domain, @PathParam("username") String username, EndUser enduser);

    /**
     * Delete user from the domain.
     *
     * @param domain
     * @param username
     * @return status
     */
    @DELETE
    @Path("{username}")
    Response delete(@PathParam("domain") String domain, @PathParam("username") String username);

}
