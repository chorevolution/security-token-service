/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package eu.chorevolution.securitytokenservice.federationserver.api;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.StringWriter;
import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.Test;

public class JSONTest {

    @Test
    public void basic() throws IOException {
        EndUser endUser = new EndUser();
        endUser.setUsername("test");
        endUser.setPassword("password");
        endUser.setActive(true);
        endUser.getChoreographies().add("wp5");
        endUser.getGroups().add("group1");
        endUser.getGroups().add("group2");
        Set<String> values = new HashSet<>();
        values.add("value1");
        values.add("value2");
        endUser.getAttributes().put("attr1", values);
        values = new HashSet<>();
        values.add("value3");
        values.add("value4");
        endUser.getAttributes().put("attr2", values);

        Map.Entry<String, String> sc1 = new AbstractMap.SimpleEntry<>("sc1_test", "password1");
        endUser.getServiceCredentials().put("sc1", sc1);

        Map.Entry<String, String> sc2 = new AbstractMap.SimpleEntry<>("sc2_test", "password2");
        endUser.getServiceCredentials().put("sc3", sc2);

        ObjectMapper mapper = new ObjectMapper();

        StringWriter writer = new StringWriter();
        mapper.writeValue(writer, endUser);

        EndUser unserialized = mapper.readValue(writer.toString(), EndUser.class);
        assertEquals(endUser, unserialized);
    }
}
